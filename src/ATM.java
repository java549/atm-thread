class ATM {
    public synchronized void checkBalance(String name) {
        System.out.println(name + " Checking Balance");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized void withdraw(String name, int amount) {
        System.out.println(name + " withdraw " + amount);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}

class Customer extends Thread {
    private final String name;
    private final int amount;
    private final ATM atm;

    Customer(String name, int amount, ATM atm) {
        this.name = name;
        this.amount = amount;
        this.atm = atm;
    }

    public void useATM() {
        atm.checkBalance(name);
        atm.withdraw(name, amount);
    }

    @Override
    public void run() {
        useATM();
    }
}
