class BlackBoard {
    int count = 0;
    int numOfStudents = 0;
    String text;

    public void increaseNumOfStudents() {
        numOfStudents++;
    }

    public synchronized void write(String text) {
        System.out.println("Teacher is writing " + text);

        while (count != 0) {
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        this.text = text;
        count = numOfStudents;
        notifyAll();
    }

    public synchronized String read() {
        while (count == 0) {
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        count--;
        if (count == 0) {
            notify();
        }
        return this.text;
    }
}

class Teacher extends Thread {
    private final BlackBoard bb;
    String[] notes = { "Hello", "World", "Test", "Google", "exit" };

    Teacher(BlackBoard bb) {this.bb = bb;}

    @Override
    public void run() {
        for (String note : notes) {
            bb.write(note);
        }
    }
}

class Student extends Thread {
    private final String name;
    private final BlackBoard bb;

    Student(String name, BlackBoard bb) {
        this.name = name;
        this.bb = bb;
    }

    @Override
    public void run() {
        String text;
        bb.increaseNumOfStudents();

        do {
            text = bb.read();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println(name + " Reading " + text);
            System.out.flush();
        } while (!text.equals("exit"));
    }
}
