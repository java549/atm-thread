public class Main {
    public static void main(String[] args) {
        /* Automatic teller machine */
        /*ATM atm = new ATM();
        Customer customer1 = new Customer("Oleh", 100, atm);
        Customer customer2 = new Customer("Ivan", 200, atm);
        customer1.start();
        customer2.start();*/
        BlackBoard bb = new BlackBoard();
        Teacher teacher = new Teacher(bb);
        Student student1 = new Student("Oleh", bb);
        Student student2 = new Student("Ivan", bb);
        Student student3 = new Student("Petro", bb);
        Student student4 = new Student("Petro1", bb);
        teacher.start();
        student1.start();
        student2.start();
        student3.start();
        student4.start();
    }
}
